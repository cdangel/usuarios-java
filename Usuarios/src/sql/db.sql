CREATE DATABASE usuarios WITH ENCODING=UNICODE OWNER postgres;

CREATE TABLE rol (
    id_rol SERIAL,
    nombre VARCHAR,
    PRIMARY KEY (id_rol)
);

CREATE TABLE usuario (
    id_usuario SERIAL,
    id_rol INT,
    nombre VARCHAR,
    activo CHAR(2),
    UNIQUE (nombre),
    PRIMARY KEY (id_usuario),
    FOREIGN KEY (id_rol) REFERENCES rol (id_rol)
);