package usuarios;

/**
 *
 * @author Carlos Daniel Angel C.
 */

import views.Ventana;

public class Usuarios {

    public static void main(String[] args) {
        Ventana ventana = new Ventana();
        ventana.setLocationRelativeTo(null);
        ventana.setTitle("Usuarios");
        ventana.setVisible(true);
    }
    
}
