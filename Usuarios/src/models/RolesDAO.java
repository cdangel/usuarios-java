package models;

import controllers.Conn;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class RolesDAO extends RolesModel {
    Conn conn;

    public RolesDAO() {
        this.conn = new Conn();
    }
    
    public RolesModel[] select() {
        try {
            String sql = "SELECT id_rol, nombre FROM rol";
            ResultSet rs = this.conn.select(sql);
            
            int i = 0;
            if (rs.last()) {
                i = rs.getRow();
                rs.beforeFirst();
            }
            
            RolesModel roles[] = new RolesModel[i];
            
            int j = 0;
            while ( rs.next() ) {
                int id_rol = rs.getInt("id_rol");
                String nombre = rs.getString("nombre");
                roles[j] = new RolesModel(id_rol, nombre);
                j++;
            }
            return roles;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error Select DAO " + e.toString());
        }
        return null;
    }
}
