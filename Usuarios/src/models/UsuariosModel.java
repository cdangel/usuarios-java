package models;

public class UsuariosModel {
    private String nombre, activo = "SI", nombre_rol;
    private int id_rol, id_usuario;

    public UsuariosModel() {
    }

    public UsuariosModel(String nombre, int id_rol, String activo) {
        this.nombre = nombre;
        this.id_rol = id_rol;
        this.activo = activo;
    }
    
    public UsuariosModel(int id_rol, String nombre_rol, int id_usuario, String nombre, String activo) {
        this.id_rol = id_rol;
        this.nombre_rol = nombre_rol;
        this.id_usuario = id_usuario;
        this.nombre = nombre;
        this.activo = activo;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId_rol() {
        return id_rol;
    }

    public void setId_rol(int id_rol) {
        this.id_rol = id_rol;
    }

    public String getNombre_rol() {
        return nombre_rol;
    }

    public void setNombre_rol(String nombre_rol) {
        this.nombre_rol = nombre_rol;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }
    
    
}
