package models;

import controllers.Conn;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class UsuariosDAO extends UsuariosModel {
    Conn conn;

    public UsuariosDAO() {
        this.conn = new Conn();
    }
    
    public boolean insert() {
        try {
            String sql = "INSERT INTO usuario (id_usuario, id_rol, nombre, activo) VALUES (DEFAULT, " + super.getId_rol() + ", '" + super.getNombre() + "', '" + super.getActivo() + "')";
            return this.conn.insert(sql);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error Insert DAO " + e.toString());
        }
        return false;
    }
    
    public UsuariosModel[] select() {
        try {
            String sql = "SELECT b.id_rol, b.nombre AS nombre_rol, a.id_usuario, a.nombre, a.activo FROM usuario AS a INNER JOIN rol AS b ON a.id_rol = b.id_rol WHERE a.nombre ILIKE '%" + super.getNombre() + "%'";
            ResultSet rs = this.conn.select(sql);
            
            int i = 0;
            if (rs.last()) {
                i = rs.getRow();
                rs.beforeFirst();
            }
            
            UsuariosModel usuarios[] = new UsuariosModel[i];
            
            int j = 0;
            while ( rs.next() ) {
                int id_rol = rs.getInt("id_rol");
                String nombre_rol = rs.getString("nombre_rol");
                int id_usuario = rs.getInt("id_usuario");
                String nombre = rs.getString("nombre");
                String activo = rs.getString("activo");
                usuarios[j] = new UsuariosModel(id_rol, nombre_rol, id_usuario, nombre, activo);
                j++;
            }
            return usuarios;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error Select DAO " + e.toString());
        }
        return null;
    }
    
    public boolean delete() {
        try {
            String sql = "DELETE FROM usuario WHERE id_usuario = " + super.getId_usuario();
            return this.conn.delete(sql);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error Insert DAO " + e.toString());
        }
        return false;
    }
    
    public boolean update() {
        try {
            String sql = "UPDATE usuario SET id_rol = " + super.getId_rol() + ", nombre = '" + super.getNombre() + "', activo = '" + super.getActivo() + "' WHERE id_usuario = " + super.getId_usuario();
            return this.conn.delete(sql);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error Insert DAO " + e.toString());
        }
        return false;
    }
}
