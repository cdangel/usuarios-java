package controllers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

public class Conn {
    String db, usr, pass;
    Connection conn;
    Statement st;
    
    public Conn() {
        this.db = "jdbc:postgresql://localhost:5432/usuarios";
        this.usr = "postgres";
        this.pass = "123";
        
        try {
            this.conn = DriverManager.getConnection(this.db, this.usr, this.pass);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error DB: " + e.toString());
        }
    }
    
    public boolean insert (String sql) {
        try {
            this.st = this.conn.createStatement();
            return st.execute(sql);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error Query: " + e.toString());
        }
        return false;
    }
    
    public boolean delete (String sql) {
        try {
            this.st = this.conn.createStatement();
            return st.execute(sql);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error Query: " + e.toString());
        }
        return false;
    }
    
    public ResultSet select (String sql) {
        try {
            this.st = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            return st.executeQuery(sql);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error Query: " + e.toString());
        }
        return null;
    }
}
